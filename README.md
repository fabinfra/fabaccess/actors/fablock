# FabLock
Python Process Actor for FabAccess
The Hardware and Software supports only non-permanent locks.
It can trigger locks, identify them via an LED and return the state of the deadbolt.

To communicate with the FabLock-Hardware MQTT is used.
Every FabLock Module must have an unique ID. The ID is 5 digits long and has leading zeros.
To distinguish the individual locks in the FabLock module, each has a 5-digit ID with leading zeros.


Since different locks have different trigger times and these must be adhered to in order to maintain the functionality of the lock, these can only be set on the hardware.

The LED flashes every 500ms seconds, when identifying. And lights permanent up when the lock is unlocked.
The state of the deadbolt is reported every 30 seconds.

## Paramters
### MQTT Configuration 
* `--host` MQTT Server Address
* `--port` MQTT Server Port
* `--user` MQTT User (optional)
* `--password` MQTT Password (optional)

### FabLock Configuration
* `--fablock` FabLock ID
* `--lock` Lock ID

## MQTT Commands
`/fablock/[FabLock-ID]/[Lock-ID]/[event]`

### Examples
* `/fablock/00001/00001/trigger`
* `/fablock/00001/00001/identify`

The state of the deadbolt is reported on:
* `/fablock/00001/00001/feedback`