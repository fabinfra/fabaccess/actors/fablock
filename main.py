#!/usr/bin/env python3
import random
import argparse
import paho.mqtt.client as mqtt
import base64
import sys

def on_raw(args, actor_name, data):
    client = mqtt.Client("fablock_actor_%05d" % random.randint(0, 99999))
    data = base64.b64decode(data + '==')

    if args.user is not None:
        if args.password is None:
            print("User set, but the password not")
            exit(-1)
        client.username_pw_set(args.user, args.password)

    client.connect(args.host, args.port)
        
    if data == b"action: unlock":
        client.publish("fablock/%05d/%05d/trigger" % (int(args.fablock), int(args.lock)), 0x00)
    elif data == b"action: identify":
        client.publish("fablock/%05d/%05d/identify" % (int(args.fablock), int(args.lock)), 0x00)
    else:
        print("Process actor called with unknown data %s" % data)
        client.disconnect()
        exit(-1)

def main(args):
    new_state = args.state
    if new_state == "free":
        pass # Do nothing
    elif new_state == "inuse":
        pass # Do nothing
    elif new_state == "tocheck":
        pass # Do nothing
    elif new_state == "blocked":
        pass # Do nothing
    elif new_state == "disabled":
        pass # Do nothing
    elif new_state == "reserved":
        pass # Do nothing
    elif new_state == "raw":
        on_raw(args, args.name, args.data)
    else:
        print("Process actor called with unknown state %s" % new_state)
        exit(-1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", help="MQTT Server Address", required=True)
    parser.add_argument("--port", help="MQTT Server Port", nargs='?', default=1883, type=int)
    parser.add_argument("--user", help="MQTT User (optional)")
    parser.add_argument("--password", help="MQTT Password (optional)")
    parser.add_argument("--fablock", help="FabLock ID", required=True)
    parser.add_argument("--lock", help="Lock ID", required=True)

    parser.add_argument("name", help="Name of Actor in BFFH")

    subparsers = parser.add_subparsers(required=True, dest="state")
    
    parser_free = subparsers.add_parser("free")
    
    parser_inuse = subparsers.add_parser("inuse")
    parser_inuse.add_argument("userid", help="The user that is now using the machine")

    parser_tocheck = subparsers.add_parser("tocheck")
    parser_tocheck.add_argument("userid", help="The user that should go check the machine")

    parser_blocked = subparsers.add_parser("blocked")
    parser_blocked.add_argument("userid", help="The user that marked the machine as blocked")

    parser_disabled = subparsers.add_parser("disabled")

    parser_reserved = subparsers.add_parser("reserved")
    parser_reserved.add_argument("userid", help="The user that reserved the machine")

    parser_raw = subparsers.add_parser("raw")
    parser_raw.add_argument("data", help="Raw data for for this actor")

    args = parser.parse_args()
    main(args)
